# MAO server

To run the server, build everything (via `make`) and run

    ./multiplexer N
    
for `N` the number of players you want. The multiplexer automatically
invokes the server code, assuming that the `maomao` executable is in
the same directory. It also should create `N` pairs of pipes in the
directory; once all have been connected to, with, for instance, the
`interact` program, the server should greet everyone.

To change program parameters, edit in `maomao.hs` any of the top-level 
values. Modifying `shortPause` changes how much time you have between actions
in a move; modifying `longPause` changes how long the server will wait for a 
move until giving up. `nInitialCards` controls the cards each player starts
with. `nHandLimit` controls the maximum number of cards in a hand. To change 
the rules, play with `baseRules` and just follow the patterns.


