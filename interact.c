#include "stdio.h"
#include "stdlib.h"

#include "sys/fcntl.h"
#include "unistd.h"

/*
 * Simple command line utility that connects to two pipes and forwards
 * standard input/output to each.
 *
 *
 */

int main(int argc, char** argv) {
    int pipe_in, pipe_out;
    if (argc == 3) {
        pipe_in = open(argv[1], O_RDONLY | O_NONBLOCK);
        pipe_out = open(argv[2], O_WRONLY);
        if (pipe_in < 0) {
            fprintf(stderr, "Could not connect to input pipe '%s'\n", argv[1]);
            return EXIT_FAILURE;
        }
        if (pipe_out < 0) {
            fprintf(stderr, "Could not connect to output pipe '%s'\n", argv[2]);
            return EXIT_FAILURE;
        }
    }
    else if (argc != 2) {
        fprintf(stderr, "Usage: interact in_named_pipe out_named_pipe\n");
        return EXIT_FAILURE;
    }

#define BUFSIZE 4096
    char buf[BUFSIZE];
    int ret;
    while (1) {
        fd_set rfds;
        FD_ZERO(&rfds);
        FD_SET(pipe_in, &rfds);
        FD_SET(STDIN_FILENO, &rfds);
        if (select(FD_SETSIZE, &rfds, NULL, NULL, NULL) < 0) {
            fprintf(stderr, "Terminated\n");
            break;
        }
        if (FD_ISSET(STDIN_FILENO, &rfds)) {
            do {
                ret = read(STDIN_FILENO, buf, BUFSIZE);
                if (ret > 0) {
                    write(pipe_out, buf, ret);
                }
            } while (ret == BUFSIZE);
            if (ret == 0) {
                fprintf(stderr, "Stdin closed\n");
                break;
            } else if (ret < 0) {
                fprintf(stderr, "Stdin broke\n");
                break;
            }
        }
        if (FD_ISSET(pipe_in, &rfds)) {
            do {
                ret = read(pipe_in, buf, BUFSIZE);
                if (ret > 0) {
                    write(STDOUT_FILENO, buf, ret);
                }
            } while (ret == BUFSIZE);
            if (ret == 0) {
                fprintf(stderr, "Pipe in closed\n");
                break;
            } else if (ret < 0) {
                fprintf(stderr, "Pipe in broke\n");
                break;
            }
        }

    }

    close(pipe_in);
    close(pipe_out);
}
