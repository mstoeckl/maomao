
all: multiplexer interact
	ghc --make -prof -fprof-auto -fprof-cafs maomao.hs

# 	ghc --make -O maomao.hs
# 	strip maomao

multiplexer: multiplexer.cpp
	g++ -Wall -std=c++11 multiplexer.cpp -o multiplexer

interact: interact.c
	gcc -Wall interact.c -o interact

clean:
	rm *.o *.hi maomao multiplexer interact
