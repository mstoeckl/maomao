// Stdlib!
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "stdarg.h"
#include "stdbool.h"
#include "math.h"

// Posix!
#include "unistd.h"
#include "fcntl.h"
#include "errno.h"
#include "time.h"

// Linux!
#include "sys/stat.h"
#include "sys/timerfd.h"

// String!
#include <string>
#include <vector>

using std::string;
using std::vector;

#define FMODE (S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH)

static int writestr(int f, const char* str)
{
    return write(f, str, strlen(str));
}

static void complain(const char* fmt, ...)
{
    static int log = 0;
    if (log == 0) {
        log = open("log.txt", O_APPEND | O_CREAT | O_WRONLY, FMODE);
        writestr(log, "\n======================================\n");
    }
    char buf[4096];
    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(buf, 4095, fmt, args);
    va_end(args);

    write(STDERR_FILENO, buf, len);
    write(log, buf, len);
}

static void cleanup(int def)
{
    complain("Cleaning up pipes\n");
    unlink("serv_in");
    unlink("serv_out");
    for (int i = 0; i < def; i++) {
        char name[16];
        snprintf(name, 16, "player_%d_in", i + 1);
        unlink(name);
        snprintf(name, 16, "player_%d_out", i + 1);
        unlink(name);
    }
}

static vector<string> readLines(int fno, string desc)
{
    const int BUFSIZE = 4096;
    static char buf[BUFSIZE];
    int r = read(fno, buf, BUFSIZE-1);
    if (r < 0) {
        complain("%s read failed\n", desc.c_str());
        return vector<string>();
    }
    else if (r == 0) {
        complain("%s is closed\n", desc.c_str());
        return vector<string>();
    }
    else if (r == BUFSIZE-1) {
        complain("%s gave too much input\n", desc.c_str());
        return vector<string>();
    }
    buf[r] = '\0';
    vector<string> opts;
    char* b = strtok(buf, "\n");
    if (!b) {
        opts.push_back("");
        return opts;
    }
    do {
        opts.push_back(string(b));
    } while ((b = strtok(NULL, "\n")) != NULL);
    return opts;
}

int main(int argc, char** argv)
{
    int def = 1;
    char* maopath = (char*) "./maomao +RTS -xc"; 
    if (argc == 2 || argc == 3) {
        int i = strtol(argv[1], NULL, 10);
        if (i > 0 && i < 10) {
            def = i;
        }
        else {
            fprintf(stderr, "Usage: multiplexer [1-9] [maopath]\n");
            return EXIT_FAILURE;
        }
        
        if (argc == 3) {
            /* may execute anything */
            maopath = argv[2];
        }
    }
    else if (argc > 3) {
        fprintf(stderr, "Usage: multiplexer [1-9] [maopath]\n");
        return EXIT_FAILURE;
    }

    complain("Hello! %s %d\n", "T", 3);
    fprintf(stderr, "Opened and wrote to log.\n");

    complain("Will run with %d players.\n", def, def, def);

    cleanup(def);

    // Note: All pipe directions are inverted;
    // Thus we write to serv_in and read from serv_out
    int server_in, server_out, timer;
    int player_in[def];
    int player_out[def];
    enum { WAITING,
        PLAYING } state
        = WAITING;

    for (int i = 0; i < def; i++) {
        // Note that open attempts may and probably will fail.
        char name[16];
        complain("Creating pipes for player %d\n", i + 1);
        snprintf(name, 16, "player_%d_in", i + 1);
        mkfifo(name, FMODE);
        player_in[i] = open(name, O_WRONLY | O_NONBLOCK);

        snprintf(name, 16, "player_%d_out", i + 1);
        mkfifo(name, FMODE);
        player_out[i] = open(name, O_RDONLY | O_NONBLOCK);
        complain("Created pipes for player %d\n", i + 1);
    }

    mkfifo("serv_in", FMODE);
    mkfifo("serv_out", FMODE);
    
    /* Temp buffer for anyone to use */
    const int TBUF = 4095;
    char obuf[TBUF+1];
    snprintf(obuf, TBUF, "%s < serv_in > serv_out &", maopath);
    system(obuf);

    complain("All pipes created.\n");

    server_in = open("serv_in", O_WRONLY);
    server_out = open("serv_out", O_RDONLY | O_NONBLOCK);



    snprintf(obuf, TBUF, "M:0:Init:%d\n", def);
    if (writestr(server_in, obuf) <= 0) {
        fprintf(stderr, "Error: %d %s\n", errno, strerror(errno));
        goto cleanup;
    }

    // Setup periodic timer. When the timer hits, attempt to reload fds
    timer = timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK);
    if (timer < 0) {
        fprintf(stderr, "Could not create timer!\n");
        goto cleanup;
    }

    /* to arm timer. */
    struct itimerspec tspec;
    tspec.it_interval.tv_sec = 0;
    tspec.it_interval.tv_nsec = 300000000;
    tspec.it_value.tv_sec = 0;
    tspec.it_value.tv_nsec = 300000000;
    if (timerfd_settime(timer, /*relative*/ 0, &tspec, NULL) < 0) {
        fprintf(stderr, "Could not set timer!\n");
        goto cleanup;
    }

    while (1) {
        // Select on the pool of FDs, which we must constantly recreate
        fd_set rfds;
        FD_ZERO(&rfds);

        // Attach all infeeds
        FD_SET(server_out, &rfds);
        FD_SET(STDIN_FILENO, &rfds);
        FD_SET(timer, &rfds);
        for (int i = 0; i < def; i++) {
            FD_SET(player_out[i], &rfds);
        }

        struct timeval timeout;
        timeout.tv_sec = 1;
        timeout.tv_usec = 250000;
        int ret = select(FD_SETSIZE, &rfds, NULL, NULL, &timeout);
        if (ret < 0) {
            complain("select failed!\n");
            goto cleanup;
        }
        else if (ret == 0) {
            continue;
        }

        // Collect a timestamp for every event
        struct timespec ct;
        clock_gettime(CLOCK_REALTIME, &ct);
        uint64_t microtime = ct.tv_sec * 1000000 + ct.tv_nsec / 1000;

        for (int i = 0; i < def; i++) {
            if (player_out[i] >= 0 && FD_ISSET(player_out[i], &rfds)) {
                // Info from player!
                vector<string> lines = readLines(player_out[i], "Player " + std::to_string(i+1));
                if (lines.empty()) {
                    goto cleanup;
                }
                for (string s : lines) {
                    if (s.empty()) {
                        continue;
                    }
                    snprintf(obuf, TBUF, "%d:%ld:%s\n", i + 1, microtime, s.c_str());
                    complain(obuf);
                    writestr(server_in, obuf);
                }
            }
        }

        if (FD_ISSET(timer, &rfds)) {
            uint64_t tt;
            if (read(timer, &tt, sizeof(uint64_t)) != sizeof(uint64_t)) {
                complain("Timer read failed!\n");
                goto cleanup;
            }
            if (tt != 1) {
                complain("Warning: Unexpected (!=1) number of timeouts: %d\n", tt);
            }
            /* Notify the server of the timeout */

            /* Do a re! check; if all are connected, turn off. */
            bool allgood = true;
            for (int i = 0; i < def; i++) {
                if (player_in[i] < 0) {
                    char name[16];
                    snprintf(name, 16, "player_%d_in", i + 1);
                    mkfifo(name, FMODE);
                    player_in[i] = open(name, O_WRONLY | O_NONBLOCK);
                    if (player_in[i] >= 0) {
                        complain("Player %d is connected! (fdesc %d)\n", i, player_in[i]);
                    }
                    else {
                        allgood = false;
                    }
                }
            }
            if (state == WAITING) {
                snprintf(obuf, TBUF, "T:%ld:Waiting:for players\n", microtime);
            }
            else {
                sprintf(obuf, "T:%ld:Timeout:\n", microtime);
            }
            writestr(server_in, obuf);
            complain(obuf);

            if (allgood) {
                if (state == WAITING) {
                    snprintf(obuf, TBUF, "M:%ld:Ready:let the game begin!\n", microtime);
                    complain(obuf);
                    writestr(server_in, obuf);
                }
                state = PLAYING;

                // Turn off timer...
                complain("Turning off timer...\n");
                tspec.it_value.tv_sec = 0;
                tspec.it_value.tv_nsec = 0;
                timerfd_settime(timer, 0, &tspec, NULL);
            }
        }
        if (FD_ISSET(STDIN_FILENO, &rfds)) {
            // Interactive override goes to server
            vector<string> lines = readLines(STDIN_FILENO, "Stdin");
            if (lines.empty()) {
                goto cleanup;
            }

            for (string s : lines) {
                if (s.empty() || s[0] == '\n') {
                    continue;
                }

                snprintf(obuf, TBUF, "S:%ld:%s\n", microtime, s.c_str());
                writestr(server_in, obuf);
                complain(obuf);
            }
        }
        if (FD_ISSET(server_out, &rfds)) {
            // Interactive override goes to server
            vector<string> lines = readLines(server_out, "Server");
            if (lines.empty()) {
                goto cleanup;
            }
            for (string s : lines) {
                if (s.length() <= 3) {
                    continue;
                }

                // Note: We assume the server returns "X:Message"
                string tail = s.substr(2, string::npos);
                char code = s[0];
                /* Select destination */
                switch (code) {
                case 'M': // Only to multiplexer
                    complain("Server to mux: %s\n", tail.c_str());
                    break;
                case 'X': // Malformed input
                    complain("Server to unknown: %s\n", tail.c_str());
                    break;
                case 'S': // To stdin
                    complain("Server to stdin: %s\n", tail.c_str());
                    break;
                case 'A': // Broadcast
                    complain("Server to all: %s\n", tail.c_str());
                    for (int i = 0; i < def; i++) {
                        writestr(player_in[i], (tail + "\n").c_str());
                    }
                    break;
                case 'T': { // To Timer
                    complain("Server to timer: %s\n", tail.c_str());
                    double time = std::strtod(tail.c_str(), NULL);
                    if (time <= 0) {
                        break;
                    }
                    // Arm the timer!
                    tspec.it_interval.tv_sec = lround(time);
                    tspec.it_interval.tv_nsec = lround(time*1e9)-1000000000*lround(floor(time));
                    tspec.it_value.tv_sec = tspec.it_interval.tv_sec;
                    tspec.it_value.tv_nsec = tspec.it_interval.tv_nsec;
                    complain("Set timer to %ld:%ld\n", tspec.it_interval.tv_sec, tspec.it_interval.tv_nsec);
                    timerfd_settime(timer, 0, &tspec, NULL);
                } break;
                case '1': // To one player
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9': {
                    int id = code - '0';
                    complain("Server to Player %d:%s\n", id, tail.c_str());
                    if (id - 1 >= def) {
                        complain("Player %d does not exist!", id);
                        goto cleanup;
                    }
                    writestr(player_in[id - 1], (tail + "\n").c_str());
                } break;
                default:
                    complain("Unidentified code: %c with message: %s\n", code, tail.c_str());
                    goto cleanup;
                }
            }
        }
    }

cleanup:
    usleep(200000);

    close(server_in);
    close(server_out);
    for (int i = 0; i < def; i++) {
        close(player_in[i]);
        close(player_out[i]);
    }
    close(timer);

    // Get rid of all the pipes, if they still are pipes
    cleanup(def);

    return argc * 3;
}
