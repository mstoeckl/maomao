 
{- 

General structure:

We read stdin as lines, modify our 'state' datastructures (rule list, cards, etc), and
and with some modifications yield things to notify which we then serialize to stdout

-}

import Data.List
import Data.Char
import Data.Maybe
import Data.Int
import System.IO
import System.Random
import Text.Read

welcomeMessage = "Welcome to the Glorious Game of Mao (♠♥♦♣)\n\
                 \Options:                                  \n\
                 \  info:           Prints this message     \n\
                 \  draw:           Draw a card             \n\
                 \  status:         Get your status         \n\
                 \  place: Jh       Place the Jack of Hearts\n\
                 \  say: Thank you  Say \"Thank you\"         \n"

-- Tune to adjust game time pressure, and minimum turn duration
shortPause = ("T:3.000000", 3000000)
longPause = ("T:50.000000", 50000000)
nInitialCards = 5
nHandLimit = 10

data Suit = Hearts | Spades | Clubs | Diamonds
    deriving (Read, Show, Enum, Eq)
data Card = Joker | Card {rank :: Int, 
                          suit :: Suit}
    deriving (Read, Eq)
instance Show Card where
    show c = cardToString c

ranks = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]

suitToString s = case s of
    Hearts -> "♥"
    Spades -> "♠"
    Clubs -> "♣"
    Diamonds -> "♦"

cardToString :: Card -> String
cardToString c = case c of
    Joker             -> "??"
    Card rank suit    -> (ranks !! (rank - 1)) ++ suitToString suit

-- May handle user input...
stringToSuit s = case toLower s of
                'h' -> Just Hearts
                'c' -> Just Clubs
                'd' -> Just Diamonds
                's' -> Just Spades
                '♥' -> Just Hearts
                '♣' -> Just Clubs
                '♦' -> Just Diamonds
                '♠' -> Just Spades
                _   -> Nothing

stringToRank s = case map toLower s of
                "a" -> Just 1
                "1" -> Just 1
                "2" -> Just 2
                "3" -> Just 3
                "4" -> Just 4
                "5" -> Just 5
                "6" -> Just 6
                "7" -> Just 7
                "8" -> Just 8
                "9" -> Just 9
                "10" -> Just 10
                "j" -> Just 11
                "q" -> Just 12
                "k" -> Just 13
                _   -> Nothing

stringToCard :: String -> Maybe Card
stringToCard s = if length s > 3 || length s < 2
                 then Nothing
                 else if s == "??"
                 then Just Joker
                 else let t = stringToSuit $ s!!(length s - 1)
                          p = stringToRank $ take (length s - 1) s
                      in if t == Nothing || p == Nothing
                         then Nothing
                         else Just (Card (fromJust p) (fromJust t) )

data Category = Wild
              | CatCard {catCard :: Card}
              | CatRank {catRank :: Int}
              | CatSuit {catSuit :: Suit}
              | Evens | Odds | Face | Numbers
              deriving (Read, Eq)

data Condition = JustPenalized
               | NumHand { num :: Int}
               | LastCard { lastCat :: Category }
               | PlacedCard { placedCat :: Category }
               deriving (Read, Eq)

data Effect = Reverse { flip :: Bool }
            | MustSay { text :: String }
            | SkipTurn { tSkip :: Int}
            | Permit { permitCat :: Category }
            | Forbid { forbidCat :: Category }
            deriving (Read, Eq)

data Rule = Rule {condition :: Condition, effect :: Effect } deriving (Read, Eq)

instance Show Rule where
    show r = "If "++show (condition r)++" then "++show (effect r)
    
instance Show Condition where
    show c = case c of
        JustPenalized -> "(justpenalized)"
        NumHand n -> "(num = "++show(n)++")"
        LastCard c -> "(last = "++show(c)++")"
        PlacedCard c -> "(placed = "++show(c)++")"

instance Show Effect where
    show e = case e of
        Reverse b -> if b then "(reverse)" else "(continue)"
        MustSay t -> "(mustsay = \""++t++"\")"
        SkipTurn i -> "(skip = "++show i++")"
        Permit c -> "(allow = "++show(c)++")"
        Forbid c -> "(block = "++show(c)++")"

instance Show Category where
    show c = case c of
        Wild -> "*"
        CatCard c -> cardToString c
        CatRank r -> ranks !! (r - 1)
        CatSuit s -> suitToString s
        Evens -> "even"
        Odds -> "odd"
        Face -> "face"
        Numbers -> "num"

-- Rules are given in reverse order: the last rule overrides all later rules
baseRules = [Rule (LastCard Wild) (Forbid Wild),
             Rule (LastCard (CatCard Joker)) (Permit Wild),
            -- Establish UNO as a baseline
             Rule (LastCard (CatSuit Hearts)) (Permit (CatSuit Hearts)),
             Rule (LastCard (CatSuit Diamonds)) (Permit (CatSuit Diamonds)),
             Rule (LastCard (CatSuit Spades)) (Permit (CatSuit Spades)),
             Rule (LastCard (CatSuit Clubs)) (Permit (CatSuit Clubs)),
             Rule (LastCard (CatRank 1)) (Permit (CatRank 1)),
             Rule (LastCard (CatRank 2)) (Permit (CatRank 2)),
             Rule (LastCard (CatRank 3)) (Permit (CatRank 3)),
             Rule (LastCard (CatRank 4)) (Permit (CatRank 4)),
             Rule (LastCard (CatRank 5)) (Permit (CatRank 5)),
             Rule (LastCard (CatRank 6)) (Permit (CatRank 6)),
             Rule (LastCard (CatRank 7)) (Permit (CatRank 7)),
             Rule (LastCard (CatRank 8)) (Permit (CatRank 8)),
             Rule (LastCard (CatRank 9)) (Permit (CatRank 9)),
             Rule (LastCard (CatRank 10)) (Permit (CatRank 10)),
             Rule (LastCard (CatRank 11)) (Permit (CatRank 11)),
             Rule (LastCard (CatRank 12)) (Permit (CatRank 12)),
             Rule (LastCard (CatRank 13)) (Permit (CatRank 13)),

-- Also -- technically we could expand rules to include (DrawnCard category)

--  We skip the JustPenalized condition for now because implementing it
--  is, quite certainly, a royal pain. For instance, JustPenalized -> Reverse
--  would enable _any_ player to reverse play order -- right?
--              Rule (JustPenalized) (MustSay "Thank you"),
             Rule (PlacedCard (CatRank 7)) (MustSay "Have a nice day!"),
             Rule (PlacedCard (CatSuit Spades)) (MustSay "Spades."),
             Rule (PlacedCard (CatRank 2)) (Reverse True),
             Rule (PlacedCard (CatRank 1)) (SkipTurn 2),
             Rule (LastCard (CatRank 11)) (Permit Wild),
-- Disabled n=1 to avoid cascading
--              Rule (NumHand 1) (MustSay "Last card."),
              Rule (NumHand 0) (MustSay "Mao!")

--               Rule (NumHand 9) (MustSay "Hi!") -- Example rule; applies when drawing too
             ]

matchCard :: Card -> Category -> Bool
matchCard card category = if card == Joker
                          then case category of
    Wild          -> True
    CatCard Joker -> True
    _             -> False
                          else case category of
    Wild      ->  True
    CatCard c ->  c == card
    CatRank r ->  r == rank card
    CatSuit s ->  s == suit card
    Evens     ->  (even $ rank card) && (rank card <= 10)
    Odds      ->  (odd $ rank card) && (rank card <= 10)
    Face      ->  rank card > 10
    Numbers   ->  rank card <= 10

data PlayDirection = Forward | Backward
                    deriving (Read, Show, Eq)

--               1s before next player starts ? 
--               5s to make a move (place/draw) after that start
--               1s _total (?)_ since card place to connect moves or
--                  rectify a penalty (which we ignore for now)
data TurnPhase = BeforeMove | DuringMove -- | DuringPenalty | AfterMove | 
                deriving (Read, Show, Eq)

randomCard :: StdGen -> (Card, StdGen)
randomCard g = let (r, g2) = randomR (1, 13) g
                   (s, g3) = randomR (0,3) g2
               in (Card r $ [Hearts, Clubs, Diamonds, Spades] !! s, g3)

-- If unique random cards are wanted, a list shuffler applied to a full
-- deck is better.
randomList :: Int -> (StdGen -> (t, StdGen)) -> StdGen -> ([t], StdGen)
randomList count fn gen = if count <= 0
                          then ([], gen)
                          else let (r, g2) = fn gen
                                   (q, g3) = randomList (count - 1) fn g2
                               in (r : q, g3)

randomCards n g = randomList n randomCard g
randomCardSets k n g = randomList k (\g -> randomCards n g) g

                
-- processIt :: [String] -> String -> (String, [String])

-- Main switching routine and initial state
-- 
--

-- Game state is decomposed into components, since functions tend to modify only 
-- one component at a time...


data CardState = CardState {lastCard :: Card,
                            hands :: [[Card]],
                            -- Note: randSource doesn't Show quite right,
                            randSource :: StdGen} deriving (Show, Read)

data MatchState = MatchState {playerCount :: Int,
                             rules :: [Rule]} deriving (Show, Read)

data PhaseState = PhaseState { turnPhase :: TurnPhase,
                               phaseTimeout :: Int64,
                                -- thingsToSay doesn't really fit; but then
                                -- once you integrate per-player timers, etc.
                               thingsToSay :: [String] } deriving (Show, Read)

data TurnState = TurnState {turn :: Int,
                            playDirection :: PlayDirection,
                            currentPlayer :: Int,
                            nextPlayer :: Int} deriving (Show, Read)

data GameState = GameState { cardState :: CardState,
                             matchState :: MatchState,
                             phaseState :: PhaseState,
                             turnState :: TurnState} deriving (Show, Read)

mkProcSeed :: StdGen -> GameState
mkProcSeed rando = newGame rando 1 (reverse baseRules) 0

newGame :: StdGen -> Int -> [Rule] -> Int -> GameState
newGame rando nplayers rules winner =     
    let (h, g) = (randomCardSets nplayers nInitialCards rando)
        cs = CardState Joker h g
        ms = MatchState nplayers rules
        ps = PhaseState BeforeMove 2305843009213693952 []
        ts = TurnState 0 Forward winner ((winner + 1) `mod` nplayers)
    in GameState cs ms ps ts 

isLegal :: [Rule] -> Card -> Card -> Bool
-- Ideally, rewrite as "judgeLegal -> Maybe" & collapse that way
isLegal (rule:rest) placedCard lastCard = case condition rule of 
    LastCard cat -> if matchCard lastCard cat
                    then case effect rule of 
                        Permit cat2 -> if matchCard placedCard cat2 then True else isLegal rest placedCard lastCard
                        Forbid cat2 -> if matchCard placedCard cat2 then False else isLegal rest placedCard lastCard
                        _           -> isLegal rest placedCard lastCard
                    else isLegal rest placedCard lastCard
    _            -> isLegal rest placedCard lastCard

condMatch rule placedCard lastCard ncards = case condition rule of 
    PlacedCard cat -> if placedCard == Nothing then False else matchCard (fromJust placedCard) cat
    LastCard cat -> matchCard lastCard cat
    NumHand num -> ncards == num

getMustSays :: [Rule] -> Maybe Card -> Card -> Int -> [String]
getMustSays [] placedCard lastCard ncards = []
getMustSays (rule:rest) placedCard lastCard ncards = 
    case effect rule of
        MustSay phrase -> (if condMatch rule placedCard lastCard ncards then [phrase] else []) ++ getMustSays rest placedCard lastCard ncards
        _              -> getMustSays rest placedCard lastCard ncards

-- Modify the hand within newhands. Yep, O(n^2) 'cause linked list
modHand player hands newhand = [if x == player then newhand else (hands !! x) | x <- [0..length hands - 1]]
dropFrom cards card = let (pre,post) = splitAt (fromJust (elemIndex card cards)) cards in pre ++ (tail post)

-- Yo! Need a Maybe Matcher
getSkipNumber [] placedCard lastCard ncards = 1
getSkipNumber (rule:rest) placedCard lastCard ncards =
    case effect rule of
         SkipTurn s -> if condMatch rule placedCard lastCard ncards then s else getSkipNumber rest placedCard lastCard ncards
         _          -> getSkipNumber rest placedCard lastCard ncards
         
getReverses [] placedCard lastCard ncards = False
getReverses (rule:rest) placedCard lastCard ncards =
    case effect rule of
         Reverse b -> if condMatch rule placedCard lastCard ncards then b else getReverses rest placedCard lastCard ncards
         _         -> getReverses rest placedCard lastCard ncards

applyTurnMods :: TurnState -> [Rule] -> Int -> Maybe Card -> Card -> Int -> TurnState
applyTurnMods ts curr_rules nplayers placedCard lastCard ncards =
    let skipno = getSkipNumber curr_rules placedCard lastCard ncards
        rev = getReverses curr_rules placedCard lastCard ncards
        nxtdir = if rev then (if (playDirection ts) == Backward then Forward else Backward) else (playDirection ts)
        cr = currentPlayer ts
        nr = nextPlayer ts
    in if nxtdir == Forward
       then TurnState (turn ts) nxtdir cr ((cr + skipno) `mod` nplayers)
       else TurnState (turn ts) nxtdir cr ((cr - skipno) `mod` nplayers)


-- Action of placing a card that one has
placeCard :: Int ->  Int64 -> GameState -> Card -> ([String], GameState)
placeCard player time state card =
    let actDesc = "A:Player "++(show (player + 1))++" places: " ++ show card
        -- Yo! Any way to refactor this stuff with the timeouts? (Answer: Yes)
        hand = getPlayerHand state player
        should_draw = isCurrentPlayer state player && turnPhase (phaseState state) == BeforeMove
        (pst, timeoutp) = if should_draw
                          then advanceTimeout (phaseState state) shortPause time DuringMove
                          else ((phaseState state), [])
        curr_rules = (rules $ matchState state)
        nplayers = (playerCount $ matchState state)
        cs = cardState $ state
    in if not should_draw
       then applyPenalty player time state "Tried to place a card at the wrong time" timeoutp
       else if isLegal curr_rules card (lastCard cs)
       then let extSays = getMustSays curr_rules (Just card) (lastCard cs) ((length hand) - 1)
                nhand = dropFrom hand card
                modhands = modHand player (hands cs) nhand
                ts = applyTurnMods (turnState state) curr_rules nplayers (Just card) (lastCard cs) ((length hand) - 1)
                pwsays = PhaseState (turnPhase pst) (phaseTimeout pst) ((thingsToSay pst)++extSays)
                nstate = GameState (CardState card modhands (randSource cs))
                                   (matchState state) pwsays ts
            in ([actDesc]++timeoutp, nstate)
       -- Apply a penalty
       else let nstate = GameState cs (matchState state) pst (turnState state)
            in applyPenalty player time nstate "Trying to place illegal card" timeoutp


-- Give a player a penalty for doing something they shouldn't have
-- Change only the cards & append a message; don't modify phases or times
applyPenalty :: Int ->  Int64 -> GameState -> String -> [String] -> ([String], GameState)
applyPenalty player time state cause prevstat = let hand = getPlayerHand state player
                                                    og = randSource $ cardState state
                                                    (nhand, ng) = if length hand >= nHandLimit
                                                                  then (hand, og)
                                                                  else let (a,g) = randomCard $ og in (hand++[a], g)
                                                    modhands = modHand player (hands $ cardState state) nhand
                                                    ptext = ["A:Player " ++ show (player+1) ++ " is penalized for: " ++ cause]
                                                    dtext = if length hand >= nHandLimit
                                                            then [show (player+1)++":You try to draw a card, but your hand is full"]
                                                            else [show (player+1)++":You draw: "++show (last nhand)]
                                                    fstate = GameState (CardState (lastCard $ cardState state) modhands ng)
                                                                (matchState state) (phaseState state) (turnState state) 
                                                in (prevstat ++ ptext ++ dtext,fstate)

isCurrentPlayer state player = currentPlayer (turnState state) == player
getPlayerHand state player = (hands $ cardState state) !! player

-- Action of drawing a new card (at any time)
drawCard :: Int -> Int64 -> GameState -> ([String], GameState)
drawCard player time state = let hand = getPlayerHand state player
                                 should_draw = isCurrentPlayer state player && turnPhase (phaseState state) == BeforeMove
                                  -- Impose a new phase timeout if should_draw
                                 (pst, timeoutp) = if should_draw
                                                   then advanceTimeout (phaseState state) shortPause time DuringMove
                                                   else ((phaseState state), [])
                        in if length hand >= nHandLimit
                        -- No penalty for full hand since it makes no difference
                        then if should_draw
                            then let nstate = GameState (cardState state) (matchState state) pst (turnState state)
                                 in (["A:Player "++(show (player+1))++" attempts to draw a card but has their hand's full"] ++ timeoutp, nstate)
                            else (["A:Player "++(show (player+1))++" attempts to draw a card but has their hand's full"] ++ timeoutp, state)
                        else let (nc, g) = randomCard $ randSource $ cardState state
                                 modhands = modHand player (hands $ cardState state) (hand ++ [nc])
                                 nstate = GameState (CardState (lastCard $ cardState state) modhands g)
                                                    (matchState state) pst (turnState state)
                                 mintext = ["A:Player "++(show (player+1))++" draws a card",
                                  (show (player+1))++":You draw: " ++ show nc] ++ timeoutp
                             in if should_draw
                                -- If we draw and should do so we invoke any rule side effects
                                then let curr_rules = rules $ matchState nstate
                                         lc = lastCard $ cardState nstate
                                         ncards =  (length hand)+1
                                         extSays = getMustSays curr_rules Nothing lc ncards
                                         ts = applyTurnMods (turnState nstate) curr_rules (playerCount $ matchState nstate) Nothing lc ncards
                                         pst = (phaseState nstate)
                                         pwsays = PhaseState (turnPhase pst) (phaseTimeout pst) ((thingsToSay pst)++extSays)
                                         fstate = GameState (cardState nstate) (matchState nstate) pwsays ts
                                     in (mintext, fstate)
                                else applyPenalty player time nstate "Drawing a card at the wrong time" (mintext)

advPlayer :: TurnState -> Int -> TurnState
advPlayer ts nplayers = let pnext_player = (nextPlayer ts)
                            aft_player = if playDirection ts == Forward
                                         then mod (pnext_player + 1) nplayers
                                         else mod (pnext_player - 1) nplayers
                            nturn = (1 + (turn ts))
                        in TurnState nturn (playDirection $ ts) pnext_player aft_player

doTimeout :: Int64 -> GameState -> ([String], GameState)
doTimeout time state = let ts = advPlayer (turnState state) (playerCount $ matchState state)
                           curr_player = (currentPlayer $ turnState state)
                           pst = phaseState state
                       in case turnPhase pst of
                  -- applyPenalty doesn't modify phase state
    BeforeMove -> let (text, nstate) = applyPenalty curr_player time state "That was your move" []
                      (advphs, timetxt) = advanceTimeout (phaseState nstate) longPause time BeforeMove
                      fstate = GameState (cardState nstate) (matchState nstate) advphs (turnState nstate)
                  in (text ++ timetxt, fstate)
    DuringMove -> let (text, nstate) = if length (thingsToSay pst) > 0
                                       then applyPenalty curr_player time state ("You forgot to say: "++show(thingsToSay (phaseState state) !! 0)) []
                                       else if length (getPlayerHand state curr_player) == 0
                                       then (["A:Player "++show(curr_player+1)++" won!",
                                              "A:Starting a new game now. Player "++show(nextPlayer ts+1)++" begins."], 
                                              newGame (randSource $ cardState state)
                                                      (playerCount $ matchState state)
                                                      (rules $ matchState state) (nextPlayer ts))
                                       else ([], state)
                      (advphs, timetxt) = advanceTimeout (phaseState nstate) longPause time BeforeMove
                      -- Wipe things to say now that move's over
                      psf = PhaseState (turnPhase advphs) (phaseTimeout advphs) []
                      fstate = GameState (cardState nstate) (matchState nstate) psf ts
                  in (text ++ timetxt, fstate)

advanceTimeout :: PhaseState -> (String, Int64) -> Int64 -> TurnPhase -> (PhaseState, [String])
advanceTimeout phaseState duration now newPhase = 
    let end = now + (snd duration)
    in (PhaseState newPhase end (thingsToSay phaseState), [(fst duration)])

sayThings :: Int -> Int64 -> GameState -> String -> ([String], GameState)
sayThings player time state text =
    let is_player = (currentPlayer $ turnState state) == player
        ps0 = (phaseState state)
        ntts = if is_player && text `elem` (thingsToSay ps0)
               then dropFrom (thingsToSay ps0) text
               else thingsToSay ps0
        ps2 = PhaseState (turnPhase ps0) (phaseTimeout ps0) (ntts)
        nstate = GameState (cardState state) (matchState state) ps2 (turnState state)
    in (["A:Player "++(show (player+1))++" says: "++text], nstate)

handForSymb :: GameState -> Char -> [Card]
handForSymb state symb = (hands $ cardState state) !! ((read [symb]) - 1)

processIt :: GameState -> (Char, Int64, String, String) -> ([String], GameState)
processIt state (pid, time,key,val) = case (pid,((readMaybe [pid] :: Maybe Int) == Nothing), key) of
    -- Failure mode: ignore input, return to same person
    (_,False,"say")   -> sayThings (read [pid] - 1) time state val
    (_,False,"draw")  -> drawCard (read [pid] - 1) time state
    (_,False,"place") -> case stringToCard val of
                   Just c -> if c `elem` handForSymb state pid
                            then placeCard (read [pid] - 1) time state c 
                            else ([pid:":You don't have that card."], state)
                   Nothing -> ([pid:":That's not a card."], state)

    (_,False,"info")  -> ((map (\x -> pid:":"++x) $ lines welcomeMessage)++
                            [pid:":There are: "++ show(playerCount $ matchState state) ++" players"]++
                            [pid:":You are: Player " ++ [pid]], state)
    
    (_,False,"status") -> ([pid:":Cards: "++show (handForSymb state pid) ++ " Last: " ++ show (lastCard $ cardState state)], state)
    
    -- Error correction
    (_,False,"")      -> ([pid:":Invalid input: \""++val++"\". Do you want to ask for \"info:\"?"], state)
    (_,False,_)      -> ([pid:":Invalid input: \""++key++": "++val++"\". Do you want to ask for \"info:\"?"], state)
    
    -- Initialize state (only multiplexer can do so)
    ('M',True,"init") -> let np = read val
                             nstate = newGame (randSource $ cardState state) np (rules $ matchState state) 0
                         in (["M:Alright"], nstate)
    -- Game is ready
    ('M',True,"ready")  -> ((map (\x -> "A:"++x) $ lines welcomeMessage) ++
                            ["A:There are: "++ show(playerCount $ matchState state) ++" players"] ++
                            [show(p)++":You are: Player "++show(p) |
                                p <- [1..(playerCount $ matchState state)]],
                            state)

    -- timer notes still waiting for players
    ('T',True,"waiting") -> ([], state)
    
    ('T',True,"timeout")  -> let timeout = (phaseTimeout $ phaseState state)
                                in if timeout <= time
                                   then doTimeout time state
                                   else (["M:We got a timeout, but it doesn't matter! (so updating to current time)",
                                    "T:"++ show((fromIntegral timeout - fromIntegral time) / 1000000.0)], state)

    -- stdin says hello
    ('S',True,"mod")       -> (["S: "++(show $ turnState state) ++ " " ++ 
                                  (show $ phaseState state) ++ " " ++
                                  (show $ cardState state) ++ " " ++
                                  (show time)], state)

    ('S',True,"pause")     -> (["T:60.00000","S:Full minute pause inserted"], state)

    -- Properly formed but meaningless
    _       -> ([pid:":Unhandled input: " ++ show (pid, time,key,val)], state)

-- transform: List transformation using hidden state.
transform :: (t -> s -> ([r], t)) -> t -> [s] -> [r]
transform op seed (r:rst) = j ++ (transform op q rst)
    where (j, q) = op seed r
transform op seed [] = []

strip :: String -> String
strip z = dropWhile isSpace $ reverse (dropWhile isSpace $ reverse z)

foursplit :: String -> (String,String,String,String)
foursplit z = let ind = [x | x <- [0..length z-1], z!!x==':']
              in (take (ind!!0) z,
                  take ((ind!!1)-(ind!!0)-1) $ drop (1+ind!!0) z,
                  take ((ind!!2)-(ind!!1)-1) $ drop (1+ind!!1) z,
                  take ((length z)-(ind!!2)-1) $ drop (1+ind!!2) z)

threesplit :: String -> (String,String,String)
threesplit z = let ind = [x | x <- [0..length z-1], z!!x==':']
              in (take (ind!!0) z,
                  take ((ind!!1)-(ind!!0)-1) $ drop (1+ind!!0) z,
                  take ((length z)-(ind!!1)-1) $ drop (1+ind!!1) z)

readOrDefault x d = case readMaybe x of
        Just z -> z
        _ -> d

colonsplit :: String -> (Char, Int64, String, String)
colonsplit z = if count ':' z == 3
    then let (a,b,c,d) = foursplit z
         in (toUpper (a !! 0), readOrDefault b 0, map toLower $ strip c, strip d)
    else if count ':' z == 2
    then let (a,b,c) = threesplit z
         in (toUpper (a !! 0), readOrDefault b 0, "", c)
    else ('X',0,"",z)

count :: Eq(t) => t -> [t] -> Int
count char string = length $ filter (\x -> x == char) string

main = do
  -- Enforce line buffered output (so pipes work)
  hSetBuffering stdout LineBuffering
  randos <- newStdGen
  s <- getContents
  let r = transform processIt (mkProcSeed randos) (map (colonsplit . unwords . words) (lines s))
  putStr (unlines r)
